pragma solidity ^0.4.2;

 /*
  * Distributor is used to show participants the exact way how the money will be spent
  * It allows to pass all the incoming transfer only to Marketing or share it between all Share accounts 
  * by specific scheme
  *
  * With owner account you specify the list of Share accounts with share values
  * then you lock Distributor. Once locked, contract cannot be unlocked and reconfigured
  *
  * For example if you add Account1 with share 10, Account2 with share 15, Account3 with share 25
  * passToShare will share 20% to Account1, 30% to Account2 and 70% to Account3
  * and passToMarketing will pass 100% of incoming transfer to Marketing
  *
  * Money is not transferred immediately, instead it's loaded to internal ballance
  * And then the Share accounts can make withdraw calls to withdraw money from balance at any time they want
  */

contract Distributor {
	// Public contract variables
	address public owner;
	uint public marketing;
	bool public isMarketingSet;
	mapping (address => uint) public accountBalances;
	mapping (address => uint) public accountShares;
	mapping (uint => address) public accountIndex;
	uint public accountCount;
	bool public isLocked;
	
	// Events that contract can fire
	event Lock();
	event OwnerChanged(address newOwner);
	event MarketingChanged(address newMarketing);
	event ShareAccountAdded(address newShareAccount, uint newShare);
	event ShareAccountRemoved(address shareAccountToRemove);
	event PaidToMarketing(uint value);
	event PaidToShare(uint value);
	event Withdraw(address receiver, uint value);
	
	function Distributor() public {
		owner = msg.sender;
		marketing = 0;
		isMarketingSet = false;
	}
	
	modifier Unlocked() { if (!isLocked) _; }
	modifier Locked() { if (isLocked) _; }
	
	// Changes the ownership when unlocked
	function changeOwner(address newOwner) public {
		if (isLocked) revert();
		if (msg.sender!=owner) revert();
		owner=newOwner;
		OwnerChanged(owner);
	}
	
	// Locks contract to prevent further reconfiguration. Lock can be done by owner only
	function lock() Unlocked public {
		if (msg.sender!=owner) revert();
		isLocked=true;
		Lock();
	}
	
	// Allows to set Marketing address. While marketing takes part in overal share process,
	// it can receive Marketing-related payments in 100% amount
	function setMarketing(address newMarketingAccount, uint newShare) Unlocked public {
		if (msg.sender!=owner) revert();
		if (!isMarketingSet){
			// If marketing hasn't been defined previously, it's added to Share account list
			marketing=accountCount;
			accountIndex[marketing]=newMarketingAccount;
			accountShares[newMarketingAccount]=newShare;
			accountBalances[newMarketingAccount]=0;
			accountCount++;
		} else {
			// If marketing has been defined previously, it will be replaced with new address provided in this call
			ShareAccountRemoved(accountIndex[marketing]);
			accountBalances[newMarketingAccount]=accountBalances[accountIndex[marketing]];
			delete accountBalances[accountIndex[marketing]];
			delete accountShares[accountIndex[marketing]];
			accountIndex[marketing]=newMarketingAccount;
			accountShares[newMarketingAccount]=newShare;
		}
		isMarketingSet=true;
		ShareAccountAdded(newMarketingAccount,newShare);
		MarketingChanged(newMarketingAccount);
	}
	
	// Adds new account to Share list
	function addShareAccount(address newShareAccount, uint newShare) Unlocked public{
		if (msg.sender!=owner) revert();
		accountIndex[accountCount]=newShareAccount;
		accountShares[newShareAccount]=newShare;
		accountBalances[newShareAccount]=0;
		accountCount++;
		ShareAccountAdded(newShareAccount,newShare);
	}
	
	// Removes account from share list. Unsets Marketing account if it has been passed to remove
	function removeShareAccount(address shareAccountToRemove) Unlocked public {
		if (msg.sender!=owner) revert();
		delete accountShares[shareAccountToRemove];
		delete accountBalances[shareAccountToRemove];
		for (uint i=0;i<accountCount;i++)
			if (accountIndex[i]==shareAccountToRemove){
				for (uint j=i;j<accountCount-1;j++)
					accountIndex[j]=accountIndex[j+1];
				accountCount--;
				delete accountIndex[accountCount];
				if ((isMarketingSet) && (i==marketing)){
					marketing=0;
					isMarketingSet=false;
				}
				i=accountCount;				
			}
		ShareAccountRemoved(shareAccountToRemove);
	}
	
	// Payable method to pass funds to Marketing account directly
	function passToMarketing() Locked payable public {
		if (msg.value<=0) revert();
		if (!isMarketingSet) revert();
		accountBalances[accountIndex[marketing]]+=msg.value;
		PaidToMarketing(msg.value);
	}
	
	// Payale method to pass funds to be shared by configures scheme between Share accounts according to their share amounts
	function passToShare() Locked payable public {
		if (msg.value<=0) revert();
		uint totalShares=0;
		for (uint i=0;i<accountCount;i++)
			totalShares+=accountShares[accountIndex[i]];
		for (i=0;i<accountCount;i++)
			accountBalances[accountIndex[i]]+=msg.value*accountShares[accountIndex[i]]/totalShares;
		PaidToShare(msg.value);
	}
	
	// Withdraw funds from Share account balance to Share account
	// Identifies Share account by sender
	function withdraw(uint _amount) Locked payable public {
		uint amount=_amount;
		if (accountBalances[msg.sender]<=0) revert();
		if (amount<=0) revert();
		if (accountBalances[msg.sender]-amount<0) revert();
		msg.sender.transfer(amount);
		accountBalances[msg.sender]-=amount;
		Withdraw(msg.sender,amount);
	}
}